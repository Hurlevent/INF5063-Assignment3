// Include system headers here


// Include local headers here
extern "C"{
#include "cuda_dsp.h"
#include "cuda_common.h"
}


#define CUDA_CHECK_ERROR(msg) \
do {\
	cudaError_t __err = cudaGetLastError(); \
if (__err != cudaSuccess) {\
	fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
	msg, cudaGetErrorString(__err), \
	__FILE__, __LINE__); \
	fprintf(stderr, "*** FAILED - ABORTING\n"); \
	exit(1); \
} \
} while (0)


__global__ void gpu_dequantize_idct_row(int16_t *in_data, uint8_t *prediction, int *w, int *h,
    uint8_t *out_data, uint8_t *quantization)
{
	int W = *w;
	//int H = *h;
	int16_t block[8 * 8];
	int y = blockIdx.x * 8;
	int x = threadIdx.x * 8;
	//input offset
	int16_t *inputoffset = in_data + y*W;
	//pridiction offset
	uint8_t *predictionoffset = prediction + y*W;
	//output offset
	uint8_t *outputoffset = out_data + y*W;

  /* Perform the dequantization and iDCT */
    int i, j;

	gpu_dequant_idct_block_8x8(inputoffset + (x * 8), block, quantization);

    for (i = 0; i < 8; ++i)
    {
      for (j = 0; j < 8; ++j)
      {
        /* Add prediction block. Note: DCT is not precise -
           Clamp to legal values */
        int16_t tmp = block[i*8+j] + (int16_t)predictionoffset[i*W+j+x];

        if(tmp < 0) { tmp = 0; }
        else if(tmp > 255) { tmp = 255; }

		outputoffset[i*W+j+x] = tmp;
      }
    }
}

void gpu_dequantize_idct(int16_t *in_data, uint8_t *prediction, uint32_t width,
	uint32_t height, uint8_t *out_data, uint8_t *quantization, int component, int mb_rows, int mb_cols)
{
	//float*out_float_data;
	int * device_width = NULL;
	int * device_height = NULL;
	int  input_width = (int)width;
	int  input_height = (int)height;
	cudaMalloc((void**)&device_width, sizeof(int));
	cudaMalloc((void**)&device_height, sizeof(int));
	cudaMemcpy(device_width, &input_width, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(device_height, &input_height, sizeof(int), cudaMemcpyHostToDevice);

	uint8_t *device_quantization = NULL;
	cudaMalloc((void**)&device_quantization, 64 * sizeof(uint8_t));
	cudaMemcpy(device_quantization, quantization, 64 * sizeof(uint8_t), cudaMemcpyHostToDevice);

	if (component == Y_COMPONENT){
		size_t sizeY = width * height;
		uint8_t * device_predicted_y;
		cudaMalloc((void **)&device_predicted_y, sizeY);
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		int16_t * device_orignal_image_y = NULL;
		cudaMalloc((void **)&device_orignal_image_y, sizeY*sizeof(int16_t));
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		uint8_t * device_out_data = NULL;
		cudaMalloc((void **)&device_out_data, sizeY);
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		cudaMemcpy(device_orignal_image_y, in_data, sizeY*sizeof(int16_t), cudaMemcpyHostToDevice);
		cudaMemcpy(device_predicted_y, prediction, sizeY, cudaMemcpyHostToDevice);
		CUDA_CHECK_ERROR("Cuda failed to memcpy");

		gpu_dequantize_idct_row<<<mb_rows,mb_cols>>>(device_orignal_image_y, device_predicted_y, device_width, device_height, device_out_data, device_quantization);
		//cudaDeviceSynchronize();
		CUDA_CHECK_ERROR("failed to execute kernel");

		cudaMemcpy(out_data, device_out_data, sizeY, cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR("failed to copy output");

		cudaFree(device_orignal_image_y);
		cudaFree(device_predicted_y);
		cudaFree(device_out_data);
	}
	else if (component == U_COMPONENT){
		size_t sizeU = width * height;

		uint8_t * device_predicted_u = NULL;
		cudaMalloc((void **)&device_predicted_u, sizeU);
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		int16_t * device_orignal_image_u = NULL;
		cudaMalloc((void **)&device_orignal_image_u, sizeU*sizeof(int16_t));
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		uint8_t * device_out_data = NULL;
		cudaMalloc((void **)&device_out_data, sizeU);
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		cudaMemcpy(device_orignal_image_u, in_data, sizeU*sizeof(int16_t), cudaMemcpyHostToDevice);
		cudaMemcpy(device_predicted_u, prediction, sizeU, cudaMemcpyHostToDevice);
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		gpu_dequantize_idct_row<<<mb_rows/2,mb_cols/2>>>(device_orignal_image_u, device_predicted_u, device_width, device_height, device_out_data, device_quantization);
		CUDA_CHECK_ERROR("failed to execute kernel");
		//cudaDeviceSynchronize();

		cudaMemcpy(out_data, device_out_data, sizeU, cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR("failed to copy output");

		cudaFree(device_orignal_image_u);
		cudaFree(device_predicted_u);
		cudaFree(device_out_data);
	}
	else{
		size_t sizeV = width * height;

		uint8_t * device_predicted_v = NULL;
		cudaMalloc((void **)&device_predicted_v, sizeV);
		CUDA_CHECK_ERROR("Cuda failed to malloc");


		int16_t * device_orignal_image_v = NULL;
		cudaMalloc((void **)&device_orignal_image_v, sizeV*sizeof(int16_t));
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		uint8_t * device_out_data = NULL;
		cudaMalloc((void **)&device_out_data, sizeV);
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		cudaMemcpy(device_orignal_image_v, in_data, sizeV*sizeof(int16_t), cudaMemcpyHostToDevice);
		cudaMemcpy(device_predicted_v, prediction, sizeV, cudaMemcpyHostToDevice);
		CUDA_CHECK_ERROR("Cuda failed to malloc");

		gpu_dequantize_idct_row<<<mb_rows/2,mb_cols/2>>>(device_orignal_image_v, device_predicted_v, device_width, device_height, device_out_data, device_quantization);
		CUDA_CHECK_ERROR("failed to execute kernel");
		//cudaDeviceSynchronize();


		cudaMemcpy(out_data, device_out_data, sizeV, cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR("failed to copy output");

		cudaFree(device_orignal_image_v);
		cudaFree(device_predicted_v);
		cudaFree(device_out_data);
	}
	cudaFree(device_quantization);
	cudaFree(device_width);
	cudaFree(device_height);

}



__global__  void gpu_dct_quantize_row(uint8_t *in_data, uint8_t *prediction, int *w, int *h,
	int16_t *out_data, uint8_t *quantization)
{
	int W = *w;
	//int H = *h;

	int16_t block[8 * 8];
	int y = blockIdx.x * 8;
	int x = threadIdx.x * 8;
	//input offset
	uint8_t *inputoffset = in_data +y*W;
	//prediction offset
	uint8_t *predictionoffset = prediction +y*W;
	//output offset
	int16_t *outputoffset = out_data + y*W;
	/* Perform the DCT and quantization */
		int i, j;

		for (i = 0; i < 8; ++i)
		{
			for (j = 0; j < 8; ++j)
			{
				block[i * 8 + j] = ((int16_t)inputoffset[i*W + j + x] - (int16_t)predictionoffset[i*W + j + x]);
			}
		}
		/* Store MBs linear in memory, i.e. the 64 coefficients are stored
		continous. This allows us to ignore stride in DCT/iDCT and other
		functions. */
		gpu_dct_quant_block_8x8(block, outputoffset + (x * 8), quantization);
}


void gpu_dct_quantize(uint8_t *in_data, uint8_t *prediction, uint32_t width,
	uint32_t height, int16_t *out_data, uint8_t *quantization, int component, int mb_rows, int mb_cols)
{
	//width and height allocation on device
	int * device_width = NULL;
	int * device_height = NULL;
	int  input_width = (int)width;
	int  input_height = (int)height;
	cudaMalloc((void**)&device_width, sizeof(int));
	cudaMalloc((void**)&device_height, sizeof(int));
	cudaMemcpy(device_width, &input_width, sizeof(int), cudaMemcpyHostToDevice);
	cudaMemcpy(device_height, &input_height, sizeof(int), cudaMemcpyHostToDevice);
	 
	uint8_t *device_quantization = NULL;
	cudaMalloc((void**)&device_quantization, 64 * sizeof(uint8_t));
	cudaMemcpy(device_quantization, quantization, 64 * sizeof(uint8_t), cudaMemcpyHostToDevice);
	
	if (component == Y_COMPONENT){
		size_t sizeY = width * height;

		// device allocation for predicted
		uint8_t * device_predicted_y;
		cudaMalloc((void **)&device_predicted_y, sizeY);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		// device allocation for orignal
		uint8_t * device_orignal_image_y = NULL;
		cudaMalloc((void **)&device_orignal_image_y, sizeY);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		// device allocation for output
		int16_t * device_out_data = NULL;
		cudaMalloc((void **)&device_out_data, sizeY*sizeof(int16_t));
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		cudaMemcpy(device_orignal_image_y, in_data, sizeY, cudaMemcpyHostToDevice);
		cudaMemcpy(device_predicted_y, prediction, sizeY, cudaMemcpyHostToDevice);
		CUDA_CHECK_ERROR("Cuda failed to memcpy");
		 
		gpu_dct_quantize_row<<<mb_rows, mb_cols>>>(device_orignal_image_y, device_predicted_y, device_width, device_height, device_out_data,device_quantization);
		//cudaDeviceSynchronize();
		CUDA_CHECK_ERROR("failed to execute kernel");
		
		//copy output back
		cudaMemcpy(out_data, device_out_data, sizeY*sizeof(int16_t), cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR("failed to copy output");

		//memory de-allocations
		cudaFree(device_orignal_image_y);
		cudaFree(device_predicted_y);
		cudaFree(device_out_data);
	}
	else if (component == U_COMPONENT){
		size_t sizeU = width * height;

		// device allocation for predicted
		uint8_t * device_predicted_u = NULL;
		cudaMalloc((void **)&device_predicted_u, sizeU);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		// device allocation for original
		uint8_t * device_orignal_image_u = NULL;
		cudaMalloc((void **)&device_orignal_image_u, sizeU);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		// device allocation for output
		int16_t * device_out_data = NULL;
		cudaMalloc((void **)&device_out_data, sizeU*sizeof(int16_t));
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		// Copy orig and predicted to device
		cudaMemcpy(device_orignal_image_u, in_data, sizeU, cudaMemcpyHostToDevice);
		cudaMemcpy(device_predicted_u, prediction, sizeU, cudaMemcpyHostToDevice);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		gpu_dct_quantize_row<<<mb_rows/2, mb_cols/2>>>(device_orignal_image_u, device_predicted_u, device_width, device_height, device_out_data,device_quantization);
		CUDA_CHECK_ERROR("failed to execute kernel");
		//cudaDeviceSynchronize();

		//copy output back
		cudaMemcpy(out_data, device_out_data, sizeU*sizeof(int16_t), cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR("failed to copy output");

		//memory de-allocations
		cudaFree(device_orignal_image_u);
		cudaFree(device_predicted_u);
		cudaFree(device_out_data);
	}
	else{
		size_t sizeV = width * height;

		// device allocation for predicted
		uint8_t * device_predicted_v = NULL;
		cudaMalloc((void **)&device_predicted_v, sizeV);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		// device allocation for orignal
		uint8_t * device_orignal_image_v = NULL;
		cudaMalloc((void **)&device_orignal_image_v, sizeV);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		// device allocation for output
		int16_t * device_out_data = NULL;
		cudaMalloc((void **)&device_out_data, sizeV*sizeof(int16_t));
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		cudaMemcpy(device_orignal_image_v, in_data, sizeV, cudaMemcpyHostToDevice);
		cudaMemcpy(device_predicted_v, prediction, sizeV, cudaMemcpyHostToDevice);
		CUDA_CHECK_ERROR("Cuda failed to malloc");
		 
		gpu_dct_quantize_row<<<mb_rows/2, mb_cols/2>>>(device_orignal_image_v, device_predicted_v, device_width, device_height, device_out_data,device_quantization);
		CUDA_CHECK_ERROR("failed to execute kernel");
		//cudaDeviceSynchronize();
		//copy output back
		cudaMemcpy(out_data, device_out_data, sizeV*sizeof(int16_t), cudaMemcpyDeviceToHost);
		CUDA_CHECK_ERROR("failed to copy output");

		//memory de-allocations
		cudaFree(device_orignal_image_v);
		cudaFree(device_predicted_v);
		cudaFree(device_out_data);
	}
	cudaFree(device_quantization);
	cudaFree(device_width);
	cudaFree(device_height);
	
}