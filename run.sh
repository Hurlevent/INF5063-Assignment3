#!/bin/bash
set -e

#
# USAGE: ./run.sh [--tegra hostname]
#

trap quit INT

function quit()
{
    echo "Cleaning up"
    ssh $TEGRA "pkill -u \$(whoami) $TEGRA_CMD" &> /dev/null || true
    ssh $PC "pkill -u \$(whoami) $PC_CMD" &> /dev/null || true
    echo "Logfiles:"
    ls -lh logs/$DATE-*.log
}

DATE=$(date -u +%Y%m%d-%H%M%S)
mkdir -p logs

SRC_DIR="$(realpath --relative-to=$HOME $(dirname $0))"

TEGRA_CMD="c63enc"
PC_CMD="c63server"

TEGRA_ARGS="/mnt/sdcard/foreman.yuv -o output -w 352 -h 288"

TEGRA_NODE=4
PC_NODE=8

#Argument parsing
while [ $# -gt 0 ] ; do
    arg=$1
    shift

    case $arg in
        --args)
            TEGRA_ARGS=$1
            shift
            ;;
        --tegra)
            TEGRA=$1
            shift
            ;;
        --pc)
            PC=$1
            shift
            ;;
        --reverse)
            T="$TEGRA_CMD"
            TEGRA_CMD="$PC_CMD"
            PC_CMD="$T"
            ;;
    esac
done

if [ -z "$PC" ]; then
    if [ "$TEGRA" == "tegra-1" ]; then
        PC="mpg-2014-13.mlab.no"
        TEGRA="tegra-1.mlab.no"
    elif [ "$TEGRA" == "tegra-2" ]; then
        PC="mpg-2014-14.mlab.no"
        TEGRA="tegra-2.mlab.no"
    elif [ "$TEGRA" == "tegra-3" ]; then
        PC="mpg-2014-17.mlab.no"
        TEGRA="tegra-3.mlab.no"
    else
        echo "unknown tegra $TEGRA"
        exit 1
    fi
fi

echo "Using $TEGRA and $PC"

#Compile on tegra and pc
echo "Compiling on tegra"
ssh $TEGRA "cd $SRC_DIR/tegra-build && make $TEGRA_CMD" || exit $?

echo "Compiling on pc"
ssh $PC "cd $SRC_DIR/x86-build && make $PC_CMD" || exit $?

#Launch on both nodes
echo "Running:"
stdbuf -oL -eL ssh $TEGRA "cd $SRC_DIR/tegra-build && stdbuf -oL -eL ./$TEGRA_CMD -r $PC_NODE $TEGRA_ARGS" |& tee logs/$DATE-tegra.log &
stdbuf -oL -eL ssh $PC "cd $SRC_DIR/x86-build && stdbuf -oL -eL ./$PC_CMD -r $TEGRA_NODE" |& tee logs/$DATE-pc.log &

wait 

echo "Done!"

quit
