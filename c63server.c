#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sisci_error.h>
#include <sisci_api.h>

#include "dolphin_common.h"
#include "server.h"
#include "tables.h"

#include "common.h"
#include "c63.h"

#include "cuda_common.h"
#include "cuda_me.h"

static uint32_t remote_node = 0;

/* getopt */
extern int optind;
extern char *optarg;



static void print_help()
{
    printf("Usage: ./c63server -r nodeid\n");
    printf("Commandline options:\n");
    printf("  -r                             Node id of client\n");
    printf("\n");
    
    exit(EXIT_FAILURE);
}

static void c63_encode_image(struct c63_common *cm, yuv_t *image)
{
    /* Advance to next frame */
    destroy_frame(cm->refframe);
    cm->refframe = cm->curframe;
    cm->curframe = create_frame(cm, image);
    
    /* Check if keyframe */
    if (cm->framenum == 0 || cm->frames_since_keyframe == cm->keyframe_interval)
    {
        cm->curframe->keyframe = 1;
        cm->frames_since_keyframe = 0;
        
        fprintf(stderr, " (keyframe) ");
    }
    else { cm->curframe->keyframe = 0; }
    
    if (!cm->curframe->keyframe)
    {
#ifndef NO_CUDA
        /* Motion Estimation */
        gpu_c63_motion_estimate(cm);
        
        /* Motion Compensation */
        gpu_c63_motion_compensate(cm);
        
#else
        
        /* Motion Estimation */
        c63_motion_estimate(cm);
        
        /* Motion Compensation */
        c63_motion_compensate(cm);
        
#endif
    }
    
#ifndef NO_CUDA
    
    /* DCT and Quantization */
    gpu_dct_quantize(image->Y, cm->curframe->predicted->Y, cm->padw[Y_COMPONENT],
                     cm->padh[Y_COMPONENT], cm->curframe->residuals->Ydct,
                     cm->quanttbl[Y_COMPONENT], Y_COMPONENT, cm->mb_rows, cm->mb_cols);
    
    gpu_dct_quantize(image->U, cm->curframe->predicted->U, cm->padw[U_COMPONENT],
                     cm->padh[U_COMPONENT], cm->curframe->residuals->Udct,
                     cm->quanttbl[U_COMPONENT], U_COMPONENT, cm->mb_rows, cm->mb_cols);
    
    gpu_dct_quantize(image->V, cm->curframe->predicted->V, cm->padw[V_COMPONENT],
                     cm->padh[V_COMPONENT], cm->curframe->residuals->Vdct,
                     cm->quanttbl[V_COMPONENT], V_COMPONENT, cm->mb_rows, cm->mb_cols);
    
    /* Reconstruct frame for inter-prediction */
    gpu_dequantize_idct(cm->curframe->residuals->Ydct, cm->curframe->predicted->Y,
                        cm->ypw, cm->yph, cm->curframe->recons->Y, cm->quanttbl[Y_COMPONENT], Y_COMPONENT, cm->mb_rows, cm->mb_cols);
    gpu_dequantize_idct(cm->curframe->residuals->Udct, cm->curframe->predicted->U,
                        cm->upw, cm->uph, cm->curframe->recons->U, cm->quanttbl[U_COMPONENT], U_COMPONENT, cm->mb_rows, cm->mb_cols);
    gpu_dequantize_idct(cm->curframe->residuals->Vdct, cm->curframe->predicted->V,
                        cm->vpw, cm->vph, cm->curframe->recons->V, cm->quanttbl[V_COMPONENT], V_COMPONENT, cm->mb_rows, cm->mb_cols);
    
#else
    
    /* DCT and Quantization */
    dct_quantize(image->Y, cm->curframe->predicted->Y, cm->padw[Y_COMPONENT],
                 cm->padh[Y_COMPONENT], cm->curframe->residuals->Ydct,
                 cm->quanttbl[Y_COMPONENT]);
    
    dct_quantize(image->U, cm->curframe->predicted->U, cm->padw[U_COMPONENT],
                 cm->padh[U_COMPONENT], cm->curframe->residuals->Udct,
                 cm->quanttbl[U_COMPONENT]);
    
    dct_quantize(image->V, cm->curframe->predicted->V, cm->padw[V_COMPONENT],
                 cm->padh[V_COMPONENT], cm->curframe->residuals->Vdct,
                 cm->quanttbl[V_COMPONENT]);
    
    /* Reconstruct frame for inter-prediction */
    dequantize_idct(cm->curframe->residuals->Ydct, cm->curframe->predicted->Y,
                    cm->ypw, cm->yph, cm->curframe->recons->Y, cm->quanttbl[Y_COMPONENT]);
    dequantize_idct(cm->curframe->residuals->Udct, cm->curframe->predicted->U,
                    cm->upw, cm->uph, cm->curframe->recons->U, cm->quanttbl[U_COMPONENT]);
    dequantize_idct(cm->curframe->residuals->Vdct, cm->curframe->predicted->V,
                    cm->vpw, cm->vph, cm->curframe->recons->V, cm->quanttbl[V_COMPONENT]);
    
#endif
    
    /* Function dump_image(), found in common.c, can be used here to check if the
     prediction is correct */
    
    write_frame(cm);
    
    ++cm->framenum;
    ++cm->frames_since_keyframe;
}

struct c63_common* init_c63_enc(int width, int height)
{
    int i;
    
    /* calloc() sets allocated memory to zero */
    struct c63_common *cm = calloc(1, sizeof(struct c63_common));
    
    cm->width = width;
    cm->height = height;
    
    cm->padw[Y_COMPONENT] = cm->ypw = (uint32_t)(ceil(width / 16.0f) * 16);
    cm->padh[Y_COMPONENT] = cm->yph = (uint32_t)(ceil(height / 16.0f) * 16);
    cm->padw[U_COMPONENT] = cm->upw = (uint32_t)(ceil(width*UX / (YX*8.0f)) * 8);
    cm->padh[U_COMPONENT] = cm->uph = (uint32_t)(ceil(height*UY / (YY*8.0f)) * 8);
    cm->padw[V_COMPONENT] = cm->vpw = (uint32_t)(ceil(width*VX / (YX*8.0f)) * 8);
    cm->padh[V_COMPONENT] = cm->vph = (uint32_t)(ceil(height*VY / (YY*8.0f)) * 8);
    
    cm->mb_cols = cm->ypw / 8;
    cm->mb_rows = cm->yph / 8;
    
    /* Quality parameters -- Home exam deliveries should have original values,
     i.e., quantization factor should be 25, search range should be 16, and the
     keyframe interval should be 100. */
    cm->qp = 25;                  // Constant quantization factor. Range: [1..50]
    cm->me_search_range = 16;     // Pixels in every direction
    cm->keyframe_interval = 100;  // Distance between keyframes
    
    /* Initialize quantization tables */
    for (i = 0; i < 64; ++i)
    {
        cm->quanttbl[Y_COMPONENT][i] = yquanttbl_def[i] / (cm->qp / 10.0);
        cm->quanttbl[U_COMPONENT][i] = uvquanttbl_def[i] / (cm->qp / 10.0);
        cm->quanttbl[V_COMPONENT][i] = uvquanttbl_def[i] / (cm->qp / 10.0);
    }
    
    cm->framenum = 0;
    cm->buffer_width = 0;
    return cm;
}

void free_c63_enc(struct c63_common* cm)
{
    destroy_frame(cm->curframe);
    free(cm);
}


int main(int argc, char **argv)
{
    int c;
    yuv_t *image;
    
    if (argc == 1) { print_help(); }
    
    while ((c = getopt(argc, argv, "h:w:o:f:i:r:")) != -1)
    {
        switch (c)
        {
            case 'r':
                remote_node = atoi(optarg);
                break;
            default:
                print_help();
                break;
        }
    }
    
    sci_init(remote_node);
    
    size_t width = 0, height = 0, image_size = 0;
    receive_width_and_height(&width, &height, &image_size);
    
    PRINT("Width: %i, Height: %i. Image_size_in_bytes: %i\n", width, height, image_size);
    
    sci_init_segments(image_size);
    
    struct c63_common *cm = init_c63_enc(width, height);
    
    while(1){
        
        unsigned int signal = wait_for_signal();
        
        if(signal == SIG_FINISHED){
            PRINT("Server done encoding, exiting...\n");
            break;
        }
        
        PRINT("Reading new frame... ");
        
        image = read_image();
        
        cm->buffer_width = 0;
        
        PRINT("Encoding...\n");
        c63_encode_image(cm, image);
        
        PRINT("Sending frame back to client...\n");
        send_encoded_image_to_client(cm);
        
        free(image->Y);
        free(image->U);
        free(image->V);
        free(image);
        
    }

    free_c63_enc(cm);
    
    sci_close();
}
