#include "client.h"

static sci_desc_t sd;

static sci_remote_segment_t remote_segment;
static sci_local_segment_t local_segment;

static sci_map_t local_map;

static unsigned int primary_adapter = 0;

static uint32_t remote_node = 0;
static uint32_t local_node = 0;

static sci_local_data_interrupt_t interrupt_from_server_to_client;
static sci_remote_data_interrupt_t interrupt_from_client_to_server;

static sci_dma_queue_t dma_queue;

static volatile void * local_segment_buffer = NULL;

static void sync(){
    sci_error_t error;

    SCICacheSync(local_map, (void*)local_segment_buffer, image_size, SCI_FLAG_CACHE_FLUSH | SCI_FLAG_CACHE_INVALIDATE, &error);
    CHECK_ERROR("SCICacheSync");
}

void send_width_and_height(uint32_t width, uint32_t height){
    sci_error_t error;

    init_sizes(width, height);

    uint32_t data[3] = {width, height, image_size};

    // The server waiting for interrupt to receive the width and height
    SCITriggerDataInterrupt(
                            interrupt_from_client_to_server,
                            &data,
                            sizeof(uint32_t)*3,
                            0,
                            &error
                            );
    CHECK_ERROR("SCITriggerDataInterrupt");
}


static void connect_interrupts(){
    sci_error_t error;

    unsigned int server_to_client_number = INT_SERVER_TO_CLIENT;
    SCICreateDataInterrupt(
                       sd,
                       &interrupt_from_server_to_client,
                       primary_adapter,
                       &server_to_client_number,
                       NULL,
                       NULL,
                       SCI_FLAG_FIXED_INTNO,
                       &error
                       );
    CHECK_ERROR("SCICreateInterrupt");

    PRINT("InterruptNr: %i\n", server_to_client_number);

    do{
        SCIConnectDataInterrupt(
                                sd,
                                &interrupt_from_client_to_server,
                                remote_node,
                                primary_adapter,
                                INT_CLIENT_TO_SERVER,
                                SCI_INFINITE_TIMEOUT,
                                0,
                                &error
                                );
    } while(error != SCI_ERR_OK);
    PRINT("Interrupt connection established!\n");
}

void sci_init(uint32_t node){
    remote_node = node;

    sci_error_t error;

    SCIInitialize(0, &error);
    CHECK_ERROR("SCIInitialize");

    SCIGetLocalNodeId(primary_adapter,
                      &local_node,
                      0,
                      &error);

    SCIOpen(&sd, 0, &error);
    CHECK_ERROR("SCIOpen");

    SCICreateDMAQueue(
                      /* segment handle */ sd,
                      /* dma handle     */ &dma_queue,
                      /* adapter number */ primary_adapter,
                      /* max entries    */ 1,
                      /* flags          */ 0,
                      /* error          */ &error
                      );
    CHECK_ERROR("SCICreateDMAQueue");

    connect_interrupts();
}

static void map_local_segment(){
    sci_error_t error;
    if(local_segment_buffer == NULL){
        local_segment_buffer = SCIMapLocalSegment(
                                                  /* segment handle */ local_segment,
                                                  /* map handle     */ &local_map,
                                                  /* offset         */ 0,
                                                  /* size           */ image_size,
                                                  /* address        */ NULL,
                                                  /* flags          */ 0,
                                                  /* error          */ &error);

        CHECK_ERROR("SCIMapLocalSegment");
    }
}

void sci_init_segments(){
    sci_error_t error;

    SCICreateSegment(sd, &local_segment, SEGMENT_CLIENT, image_size, NULL, NULL, 0, &error);
    CHECK_ERROR("SCICreateSegment");

    SCIPrepareSegment(local_segment, primary_adapter, 0, &error);
    CHECK_ERROR("SCIPrepareSegment");

    SCISetSegmentAvailable(local_segment, primary_adapter, 0, &error);
    CHECK_ERROR("SCISetSegmentAvailable");

    /// Connecting to segment on the server
    PRINT("Connecting\n");
    do {
        PRINT(".");
        SCIConnectSegment(
                          /* SCI descrptr */  sd,
                          /* Segment (out)*/  &remote_segment,
                          /* Node Id      */  remote_node,
                          /* Segment Id   */  SEGMENT_SERVER,
                          /* Adapter Nr   */  primary_adapter,
                          /* Callback     */  NULL,
                          /* Callback arg */  NULL,
                          /* Timeout      */  SCI_INFINITE_TIMEOUT,
                          /* Flags        */  0,
                          /* Error (out)  */  &error);
        if(error == SCI_ERR_ILLEGAL_PARAMETER)
            fprintf(stderr, "SCIConnectSegment failed: %s\n", SCIGetErrorString(error));
    } while (error != SCI_ERR_OK);
    PRINT("Connection established!\n");

    map_local_segment();
}


void sci_close(){
    sci_error_t error;
    
    local_segment_buffer = NULL;
    
    SCIUnmapSegment(local_map, 0, &error);
    CHECK_ERROR("SCIUnmapSegment");
    
    SCIDisconnectSegment(remote_segment, 0, &error);
    CHECK_ERROR("SCIDisconnectSegment");
    
    SCISetSegmentUnavailable(local_segment, primary_adapter, 0, &error);
    CHECK_ERROR("SCISetSegmentUnavailable");
    
    SCIRemoveSegment(local_segment, SCI_FLAG_FORCE_REMOVE, &error);
    CHECK_ERROR("SCIRemoveSegment");
    
    SCIDisconnectDataInterrupt(interrupt_from_client_to_server, 0, &error);
    CHECK_ERROR("SCIDisconnectDataInterrupt");
    
    SCIRemoveDataInterrupt(interrupt_from_server_to_client, 0, &error);
    CHECK_ERROR("SCIRemoveInterrupt");
    
    SCIRemoveDMAQueue(dma_queue, 0, &error);
    CHECK_ERROR("SCIRemoveDMAQueue");
    
    SCIClose(sd, 0, &error);
    CHECK_ERROR("SCIClose");
    
    SCITerminate();
}

// Used as a callback function for the dma transfer in "tranfer_frame"
static sci_callback_action_t dma_callback(void * argument, sci_dma_queue_t dma_queue, sci_error_t error) {
    sci_callback_action_t next_action;
    if (error == SCI_ERR_OK) {

        // Triggers interrupt on server, and tells it to continue execution
        send_signal(SIG_CONTINUE);

        next_action = SCI_CALLBACK_CONTINUE;

    } else {
        next_action = SCI_CALLBACK_CANCEL;
    }

    return next_action;
}

void transfer_frame(){
    sci_error_t error;

    sync();

    int callback_arg = 0;

    // Transfers the contents of the local segment to the segment on the server
    SCIStartDmaTransfer(
                        /*   dma handle   */ dma_queue,
                        /*   localSeg     */ local_segment,
                        /*   remoteSeg    */ remote_segment,
                        /*   localOffset  */ 0,
                        /*   size         */ image_size,
                        /*   remoteOffset */ 0,
                        /*   callback     */ dma_callback,
                        /*   callback arg */ &callback_arg,
                        /*   flags        */ SCI_FLAG_USE_CALLBACK,
                        /*   error        */ &error
                        );

    CHECK_ERROR("SCIStartDmaTransfer");
}

void send_signal(unsigned int signal){
    sci_error_t error;
 
    unsigned int data = signal;
    SCITriggerDataInterrupt(interrupt_from_client_to_server, &data, sizeof(data), 0, &error);
    CHECK_ERROR("SCITriggerDataInterrupt");
}

unsigned int wait_for_encoded_image(){
    sci_error_t error;

    // The amount of frames received on the dma transfer from the server to client
    unsigned int data = -1;

    unsigned int size = sizeof(unsigned int);
    
    SCIWaitForDataInterrupt(interrupt_from_server_to_client, &data, &size, SCI_INFINITE_TIMEOUT,
                        0, &error);
    CHECK_ERROR("SCIWaitForDataInterrupt");
    
    return data;
}

size_t read_frame(FILE *file)
{
    if(local_segment_buffer == NULL){
        return -1;
    }
    
    size_t len = 0;
    unsigned int offset = 0;
    
    /* Read Y. The size of Y is the same as the size of the image. The offset values are padded to the nearest multiple of 8 */
    
    len += fread(local_segment_buffer + offset, 1, image_width*image_height, file);
    
    offset += ypw * yph;
    
    /* Read U. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y
     because (height/2)*(width/2) = (height*width)/4. */
    
    len += fread(local_segment_buffer + offset, 1, (image_width*image_height)/4, file);
    
    offset += upw * uph;
    
    /* Read V. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y. */
    
    len += fread(local_segment_buffer + offset, 1, (image_width*image_height)/4, file);
    
    offset += vpw * vph;
    
    if (ferror(file))
    {
        perror("ferror");
        exit(EXIT_FAILURE);
    }
    
    if(feof(file)){
        return len;
    }
    
    if (len != image_width*image_height*1.5)
    {
        fprintf(stderr, "Reached end of file, but incorrect bytes read.\n");
        fprintf(stderr, "Wrong input? (height: %d width: %d)\n", image_height, image_width);
        return -1;
    }
    
    return len;
}


void write_encoded_frame(FILE * file, unsigned int length){
    sync();
    // Directly writes the contents of the buffer to file
    fwrite((void *)local_segment_buffer, 1, length, file);
    sync();
}
