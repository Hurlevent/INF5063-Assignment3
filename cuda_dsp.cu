#include <inttypes.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <math_functions.h>
extern "C"{
  #include "cuda_dsp.h"
  #include "tables.h"
}
__device__ __constant__  int zigzagBlock[64] = { 0, 1, 8, 16, 9, 2, 3, 10, 17, 24, 32, 25, 18, 11, 4, 5, 12, 19, 26, 33, 40, 48, 41, 34, 27, 20, 13, 6, 7, 14, 21, 28, 35, 42, 49, 56, 57, 50, 43, 36, 29, 22, 15, 23, 30, 37, 44, 51, 58, 59, 52, 45, 38, 31, 39, 46, 53, 60, 61, 54, 47, 55, 62, 63 };


__device__ __constant__ float dev_dctlookup[8][8] =
  {
    { 1.0f, 0.980785f, 0.923880f, 0.831470f, 0.707107f, 0.555570f, 0.382683f, 0.195090f, },
    { 1.0f, 0.831470f, 0.382683f, -0.195090f, -0.707107f, -0.980785f, -0.923880f, -0.555570f, },
    { 1.0f, 0.555570f, -0.382683f, -0.980785f, -0.707107f, 0.195090f, 0.923880f, 0.831470f, },
    { 1.0f, 0.195090f, -0.923880f, -0.555570f, 0.707107f, 0.831470f, -0.382683f, -0.980785f, },
    { 1.0f, -0.195090f, -0.923880f, 0.555570f, 0.707107f, -0.831470f, -0.382683f, 0.980785f, },
    { 1.0f, -0.555570f, -0.382683f, 0.980785f, -0.707107f, -0.195090f, 0.923880f, -0.831470f, },
    { 1.0f, -0.831470f, 0.382683f, 0.195090f, -0.707107f, 0.980785f, -0.923880f, 0.555570f, },
    { 1.0f, -0.980785f, 0.923880f, -0.831470f, 0.707107f, -0.555570f, 0.382683f, -0.195090f, },
  };
//__device__ float g_mb[64];
//__device__ float g_mb2[64];
__device__ static void gpu_transpose_block(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
    {
      for (j = 0; j < 8; ++j)
	{
	  out_data[i*8+j] = in_data[j*8+i];
	}
    }
}
__global__ static void kernel_transpose_block(float *in_data, float *out_data)
{
  int i, j;
  i = threadIdx.x ;
  j = blockIdx.x ;
  out_data[i * 8 + j] = in_data[j * 8 + i];
}
__device__ static void gpu_dct_1d(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
    {
      float dct = 0;

      for (j = 0; j < 8; ++j)
	{
	  dct += in_data[j] * dev_dctlookup[j][i];
	}

      out_data[i] = dct;
    }
}

__device__ static void gpu_idct_1d(float *in_data, float *out_data)
{
  int i, j;

  for (i = 0; i < 8; ++i)
    {
      float idct = 0;

      for (j = 0; j < 8; ++j)
	{
	  idct += in_data[j] * dev_dctlookup[i][j];
	}

      out_data[i] = idct;
    }
}

__device__ static void gpu_scale_block(float *in_data, float *out_data)
{
  int u, v;

  for (v = 0; v < 8; ++v)
    {
      for (u = 0; u < 8; ++u)
	{
	  float a1 = !u ? ISQRT2 : 1.0f;
	  float a2 = !v ? ISQRT2 : 1.0f;

	  /* Scale according to normalizing function */
	  out_data[v*8+u] = in_data[v*8+u] * a1 * a2;
	}
    }
}

__device__ static void gpu_quantize_block(float *in_data, float *out_data, uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
    {
      float dct = in_data[zigzagBlock[zigzag]];
      /* Zig-zag and quantize */
      out_data[zigzag] = roundf((dct / 4.0) / quant_tbl[zigzag]);
    }
}

__device__ static void gpu_dequantize_block(float *in_data, float *out_data,
					    uint8_t *quant_tbl)
{
  int zigzag;

  for (zigzag = 0; zigzag < 64; ++zigzag)
    {
      float dct = in_data[zigzag];
      /* Zig-zag and de-quantize */
      out_data[zigzagBlock[zigzag]] = roundf((dct * quant_tbl[zigzag]) / 4.0);
    }
}

__device__ int floata2int(double d)
{
  union Cast
  {
    double d;
    long l;
  };
  volatile Cast c;
  c.d = d + 6755399441055744.0;
  return c.l;
}
// Has definition in header
__device__ void gpu_dct_quant_block_8x8(int16_t *in_data, int16_t *out_data,
					uint8_t *quant_tbl)
{
  float mb[8 * 8]  __attribute((aligned(16)));
  float mb2[8 * 8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb2[i] = (float)in_data[i];
  }
  //acclocate mb and mb2 for child kernels
  //memcpy(g_mb, mb, sizeof(float)* 64 );
  //memcpy(g_mb2, mb2, sizeof(float)* 64);
  //float *child_kernel_mb;
  //float *child_kernel_mb2;
  //cudaMalloc((void**)&child_kernel_mb, 64 * sizeof(float));
  //cudaMemcpy(child_kernel_mb, mb, 64 * sizeof(float), cudaMemcpyDeviceToDevice);

  //cudaMalloc((void**)&child_kernel_mb2, 64 * sizeof(float));
  //cudaMemcpy(child_kernel_mb2, mb2, 64 * sizeof(float), cudaMemcpyDeviceToDevice);
  /* Two 1D DCT operations with transpose */
  for (v = 0; v < 8; ++v) { gpu_dct_1d(mb2+v*8, mb+v*8); }
  gpu_transpose_block(mb, mb2);
  for (v = 0; v < 8; ++v) { gpu_dct_1d(mb2+v*8, mb+v*8); }
  gpu_transpose_block(mb, mb2);

  gpu_scale_block(mb2, mb);

  gpu_quantize_block(mb, mb2, quant_tbl);
  //cudaMemcpy(mb, child_kernel_mb, 64 * sizeof(float), cudaMemcpyDeviceToDevice);
  //cudaMemcpy(mb2, child_kernel_mb2, 64 * sizeof(float), cudaMemcpyDeviceToDevice);
  //cudaFree(child_kernel_mb);
  //cudaFree(child_kernel_mb2);
  //int16_t outputer[64];
  for (i = 0; i < 64; ++i) {
    out_data[i] = mb2[i];
  }
}

// Has definition in header
__device__ void gpu_dequant_idct_block_8x8(int16_t *in_data, int16_t *out_data,
					   uint8_t *quant_tbl)
{
  float mb[8*8] __attribute((aligned(16)));
  float mb2[8*8] __attribute((aligned(16)));

  int i, v;

  for (i = 0; i < 64; ++i) { mb[i] = in_data[i]; }

  gpu_dequantize_block(mb, mb2, quant_tbl);
  gpu_scale_block(mb2, mb);

  /* Two 1D inverse DCT operations with transpose */
  for (v = 0; v < 8; ++v) { gpu_idct_1d(mb+v*8, mb2+v*8); }
  gpu_transpose_block(mb2, mb);
  for (v = 0; v < 8; ++v) { gpu_idct_1d(mb+v*8, mb2+v*8); }
  gpu_transpose_block(mb2, mb);

  for (i = 0; i < 64; ++i) { out_data[i] = floata2int(mb[i]); }
}

// Has definition in header
void gpu_sad_block_8x8(uint8_t *block1, uint8_t *block2, int stride, int *result)
{
  int u, v;

  *result = 0;

  for (v = 0; v < 8; ++v)
    {
      for (u = 0; u < 8; ++u)
	{
	  *result += abs(block2[v*stride+u] - block1[v*stride+u]);
	}
    }
}
