#include <assert.h>
#include <errno.h>
#include <getopt.h>
#include <limits.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <sisci_error.h>
#include <sisci_api.h>

#include "dolphin_common.h"
#include "client.h"
#include "c63.h"
#include "c63_write.h"
#include "common.h"
#include "me.h"
#include "tables.h"

static char *output_file, *input_file;
FILE *outfile;

static int limit_numframes = 0;

static uint32_t width;
static uint32_t height;
static uint32_t remote_node = 0;

/* getopt */
extern int optind;
extern char *optarg;


static void print_help()
{
  printf("Usage: ./c63enc [options] input_file\n");
  printf("Commandline options:\n");
  printf("  -h                             Height of images to compress\n");
  printf("  -w                             Width of images to compress\n");
  printf("  -o                             Output file (.c63)\n");
  printf("  -r                             Node id of server\n");
  printf("  [-f]                           Limit number of frames to encode\n");
  printf("\n");

  exit(EXIT_FAILURE);
}

int main(int argc, char **argv)
{
  int c;
  yuv_t *image;
    
  time_t timer = time(NULL);

  if (argc == 1) { print_help(); }

  while ((c = getopt(argc, argv, "h:w:o:f:i:r:")) != -1)
  {
    switch (c)
    {
      case 'h':
        height = atoi(optarg);
        break;
      case 'w':
        width = atoi(optarg);
        break;
      case 'o':
        output_file = optarg;
        break;
      case 'f':
        limit_numframes = atoi(optarg);
        break;
      case 'r':
        remote_node = atoi(optarg);
        break;
      default:
        print_help();
        break;
    }
  }

  if (optind >= argc)
  {
    fprintf(stderr, "Error getting program options, try --help.\n");
    exit(EXIT_FAILURE);
  }

  outfile = fopen(output_file, "wb");

  if (outfile == NULL)
  {
    perror("fopen output file");
    exit(EXIT_FAILURE);
  }

  input_file = argv[optind];

  if (limit_numframes) { PRINT("Limited to %d frames.\n", limit_numframes); }

  FILE *infile = fopen(input_file, "rb");

  if (infile == NULL)
  {
    perror("fopen input file");
    exit(EXIT_FAILURE);
  }

  sci_init(remote_node);
    
  send_width_and_height(width, height);
    
  sci_init_segments();

  /* Encode input frames */
  int numframes = 0;
  while (1){

	  int done = read_frame(infile);

	  if (done <= 0) { break; }

      PRINT("Sending frame %d to server...\n", numframes);
	  
      transfer_frame();

      unsigned int length = wait_for_encoded_image();

      
      PRINT("Writing frame %d to file.\n\n", numframes);
      
      write_encoded_frame(outfile, length);

	  ++numframes;

	  if (limit_numframes && numframes >= limit_numframes) { break; }
  }
  send_signal(SIG_FINISHED);
  
  fclose(outfile);
  fclose(infile);
    
  PRINT("Client Exiting! Time spent: %i seconds\n", time(NULL) - timer);

  sci_close();

  return EXIT_SUCCESS;
}
