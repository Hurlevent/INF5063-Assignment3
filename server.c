#include "server.h"


static sci_desc_t sd;

static sci_local_segment_t local_segment;
static sci_remote_segment_t remote_segment;


static sci_remote_data_interrupt_t interrupt_from_server_to_client;
static sci_local_data_interrupt_t interrupt_from_client_to_server;

unsigned int primary_adapter = 0;

static uint32_t remote_node = 0;
static uint32_t local_node = 0;

static sci_map_t local_map;
static sci_dma_queue_t dma_queue;

volatile void * local_segment_buffer = NULL;

static void connect_interrupts(){
    sci_error_t error;
    
    sci_local_data_interrupt_t * local_interrupt;

    unsigned int client_to_server_number = INT_CLIENT_TO_SERVER;

    SCICreateDataInterrupt(
                       sd,
                       &interrupt_from_client_to_server,
                       primary_adapter,
                       &client_to_server_number,
                       NULL,
                       NULL,
                       SCI_FLAG_FIXED_INTNO,
                       &error
                       );
    
    CHECK_ERROR("SCICreateDataInterrupt");
    
    PRINT("interruptNr: %i\n", client_to_server_number);
    
    do{
        SCIConnectDataInterrupt(
                        sd,
                        &interrupt_from_server_to_client,
                        remote_node,
                        primary_adapter,
                        INT_SERVER_TO_CLIENT,
                        SCI_INFINITE_TIMEOUT,
                        0,
                        &error
                        );
    } while(error != SCI_ERR_OK);
    PRINT("Interrupt connection established!\n");
}

void sci_init(uint32_t node){
    remote_node = node;
    
    sci_error_t error;

    SCIInitialize(0, &error);
    CHECK_ERROR("SCIInitialize");

    SCIGetLocalNodeId(primary_adapter,
                      &local_node,
                      0,
                      &error);
    
    SCIOpen(&sd, 0, &error);
    CHECK_ERROR("SCIOpen");

    // Create and connect interrupts
    connect_interrupts();
    
    SCICreateDMAQueue(
                      /* segment handle */ sd,
                      /* dma handle     */ &dma_queue,
                      /* adapter number */ primary_adapter,
                      /* max entries    */ 1,
                      /* flags          */ 0,
                      /* error          */ &error
                      );
    CHECK_ERROR("SCICreateDMAQueue");
}

static void map_local_segment(){
    sci_error_t error;
    if(local_segment_buffer == NULL){
        local_segment_buffer =           SCIMapLocalSegment(
                                                            /* segment handle  */ local_segment,
                                                            /* map handle      */ &local_map,
                                                            /* offset          */ 0,
                                                            /* size            */ image_size,
                                                            /* address         */ NULL,
                                                            /* flags           */ 0,
                                                            /* error           */ &error
                                                            );
        CHECK_ERROR("SCIMapLocalSegment");
    }
}

void sci_init_segments(){
    sci_error_t error;
    
    SCICreateSegment(sd, &local_segment, SEGMENT_SERVER, image_size, NULL, NULL, 0, &error);
    CHECK_ERROR("SCICreateSegment");
    
    SCIPrepareSegment(local_segment, primary_adapter, 0, &error);
    CHECK_ERROR("SCIPrepareSegment");
    
    SCISetSegmentAvailable(local_segment, primary_adapter, 0, &error);
    CHECK_ERROR("SCISetSegmentAvailable");

    // Connects to the client segment
    PRINT("Connecting.\n");
    do {
        SCIConnectSegment(
                          /* SCI descriptor*/ sd,
                          /* Segment       */ &remote_segment,
                          /* Node Id       */ remote_node,
                          /* Segment Id    */ SEGMENT_CLIENT,
                          /* Adapter Nr    */ primary_adapter,
                          /* Callback      */ NULL,
                          /* Callback arg  */ NULL,
                          /* Timeout       */ SCI_INFINITE_TIMEOUT,
                          /* Flags         */ 0,
                          /* Error         */ &error);
        if(error == SCI_ERR_ILLEGAL_PARAMETER)
            fprintf(stderr, "SCIConnectSegment failed: %s\n", SCIGetErrorString(error));
    } while (error != SCI_ERR_OK);

    PRINT("Connection established!\n");

    map_local_segment();
}


void sci_close(){
    sci_error_t error;
    
    local_segment_buffer = NULL;
    
    SCIUnmapSegment(local_map, 0, &error);
    CHECK_ERROR("SCIUnmapSegment");
    
    SCIDisconnectSegment(remote_segment, 0, &error);
    CHECK_ERROR("SCIDisconnectSegment");
    
    SCISetSegmentUnavailable(local_segment, primary_adapter, 0, &error);
    CHECK_ERROR("SCISetSegmentUnavailable");
    
    SCIRemoveSegment(local_segment, SCI_FLAG_FORCE_REMOVE, &error);
    CHECK_ERROR("SCIRemoveSegment");
    
    SCIDisconnectDataInterrupt(interrupt_from_server_to_client, 0, &error);
    CHECK_ERROR("SCIDisconnectDataInterrupt");
    
    SCIRemoveDataInterrupt(interrupt_from_client_to_server, 0, &error);
    
    SCIRemoveDMAQueue(dma_queue, 0, &error);
    CHECK_ERROR("SCIRemoveDMAQueue");
    
    SCIClose(sd, 0, &error);
    CHECK_ERROR("SCIClose");
    
    SCITerminate();
}

void receive_width_and_height(uint32_t * width, uint32_t * height, uint32_t * frame_size){
    sci_error_t error;
    
    uint32_t data[3];
    unsigned int length = sizeof(data);

    SCIWaitForDataInterrupt(interrupt_from_client_to_server,
                            &data,
                            &length,
                            SCI_INFINITE_TIMEOUT,
                            0,
                            &error);

    CHECK_ERROR("SCIWaitForDataInterrupt");

    // Writes to output parameters
    (*width) = data[0];
    (*height) = data[1];
    (*frame_size) = data[2];

    // Initialize global variables in dolphin_common.c translation unit
    init_sizes(data[0], data[1]);
}

unsigned int wait_for_signal(){
    sci_error_t error;
    
    unsigned int status;
    unsigned int size = sizeof(status);

    SCIWaitForDataInterrupt(interrupt_from_client_to_server, &status, &size, SCI_INFINITE_TIMEOUT, 0, &error);
    CHECK_ERROR("SCIWaitForDataInterrupt");
    
    return status;
}

static void signal_client(unsigned int signal){
    sci_error_t error;
    
    int data = signal;
    SCITriggerDataInterrupt(interrupt_from_server_to_client, &data, sizeof(data), 0, &error);
    CHECK_ERROR("SCITriggerInterrupt");
}

yuv_t * read_image(){
    yuv_t *image = malloc(sizeof(yuv_t));
    
    unsigned int offset = 0;
    unsigned int size_luma = image_width * image_height;
    unsigned int size_chroma = (image_width * image_height) / 4;
    
    /* Read Y. The size of Y is the same as the size of the image. But the buffer size needs to be padded to the nearest multiple of 8 */
    
    unsigned int padded_size_y = ypw * yph;
    image->Y = calloc(1, padded_size_y);
    memcpy(image->Y, local_segment_buffer + offset, size_luma);
    offset += padded_size_y;
    
    /* Read U. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y
     because (height/2)*(width/2) = (height*width)/4. The buffer size must be padded to the nearest multiple of 8*/
    
    unsigned int padded_size_u = upw * uph;
    image->U = calloc(1, padded_size_u);
    memcpy(image->U, local_segment_buffer + offset, size_chroma);
    offset += padded_size_u;
    
    /* Read V. Given 4:2:0 chroma sub-sampling, the size is 1/4 of Y. Buffer size is padded to the nearest multiple of 8*/
    
    unsigned int padded_size_v = vpw * vph;
    image->V = calloc(1, padded_size_v);
    memcpy(image->V, local_segment_buffer + offset, size_chroma);
    
    return image;
}

// Used as a callback function for the dma transfer in "send_encoded_image_to_client"
static sci_callback_action_t dma_callback(void * argument, sci_dma_queue_t dma_queue, sci_error_t error) {
    sci_callback_action_t next_action;
    if (error == SCI_ERR_OK) {

        // number of bytes tranfered in dma
        unsigned int bytes_transfered = ((struct c63_common *)argument)->buffer_width;

        // Send interrupt to client, to notify it that the DMA transfer is complete, and it can begin reading encoded frames
        signal_client(bytes_transfered);

        next_action = SCI_CALLBACK_CONTINUE;

    } else {
        next_action = SCI_CALLBACK_CANCEL;
    }

    return next_action;
}


void send_encoded_image_to_client(const struct c63_common * cm){
    sci_error_t error;

    memcpy(local_segment_buffer, cm->dataBuffer, cm->buffer_width);

    SCICacheSync(local_map, local_segment_buffer, sizeof(image_size), SCI_FLAG_CACHE_FLUSH | SCI_FLAG_CACHE_INVALIDATE, &error);
    CHECK_ERROR("SCICacheSync");

    SCIStartDmaTransfer(dma_queue,
                        local_segment,
                        remote_segment,
                        0,
                        cm->buffer_width,
                        0,
                        dma_callback,
                        cm,
                        SCI_FLAG_USE_CALLBACK,
                        &error);
    CHECK_ERROR("SCIStartDmaTransfer");
}
