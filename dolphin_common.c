#include <sisci_error.h>
#include <sisci_api.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include "dolphin_common.h"



void sci_check_error(const char * pretty_function, sci_error_t error, const char * file, const char * function){
  if(error != SCI_ERR_OK) {
    fprintf(stderr,"Failure in function: %s (%s) called by %s\nError: %s\n", pretty_function, file, function, SCIGetErrorString(error));
    exit(EXIT_FAILURE);
  }
}


static void PrintParameters(unsigned int client,
	unsigned int localNodeId,
	unsigned int remoteNodeId,
	unsigned int localAdapterNo,
	size_t segmentSize)
{

	printf("Test parameters for %s \n", (client) ? "client" : "server");
	printf("----------------------------\n\n");
	printf("Local node-id     : %u\n", localNodeId);
	printf("Remote node-id    : %u\n", remoteNodeId);
	printf("Local adapter no. : %u\n", localAdapterNo);
	printf("Block size        : %llu\n", (unsigned long long) segmentSize);
	printf("----------------------------\n\n");
}


size_t image_size;
uint32_t ypw, yph, upw, uph, vpw, vph;
unsigned int image_width, image_height;

void init_sizes(int width, int height){
    
    ypw = (uint32_t)(ceil(width/16.0f)*16);
    yph = (uint32_t)(ceil(height/16.0f)*16);
    upw = (uint32_t)(ceil(width*UX/(YX*8.0f))*8);
    uph = (uint32_t)(ceil(height*UY/(YY*8.0f))*8);
    vpw = (uint32_t)(ceil(width*VX/(YX*8.0f))*8);
    vph = (uint32_t)(ceil(height*VY/(YY*8.0f))*8);
    
    image_width = width;
    image_height = height;
    
    image_size = ypw * yph + upw * uph + vpw * vph;
}
