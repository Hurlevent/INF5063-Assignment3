#ifndef _SERVER_H_
#define _SERVER_H_

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sisci_api.h>
#include <sisci_error.h>

#include "dolphin_common.h"

#define PRINT_COLOR "\x1B[31m"

///
/// Initializes SISCI descriptors, interrupts, and DMA queues
///
void sci_init(uint32_t node);

///
/// Initializes and maps local segment.
/// Also connects to remote segment.
/// Segment size is based off width and height received from "receive_width_and_height"
///
void sci_init_segments();

///
/// Closes all SISCI descriptors, disconnect all remote connections, closes all segments, interrupts and DMA queues
///
void sci_close();

///
/// Blocks the process until a data interrupt is received
/// Should be used at the start of the program, in order to receive the width and height of the video that is being encoded.
///
void receive_width_and_height(uint32_t * width, uint32_t * height, uint32_t * frame_size);

///
/// Blocks the program until a data interrupt is received from the client.
/// Should be used before trying to read YUV frames from the local segment.
/// The return value can be either SIG_CONTINUE or SIG_FINISHED.
///
unsigned int wait_for_signal();

///
/// Begins a DMA transfer of the encoded frame to the client.
/// A callback function will trigger a data interrupt to the client when the DMA tranfer has completed, the data sent with the interrupt is an integer with its value being the number of bytes tranfered.
/// Should only be used after a frame has been encoded
///
void send_encoded_image_to_client(const struct c63_common * cm);

///
/// Interprerets the data in the local segment as an YUV frame.
/// It reads the contents of the local segment and allocates memory on the heap, which it then writes the YUV data to.
///
yuv_t * read_image();


#endif // _SERVER_H_
