#ifndef _CLIENT_H_
#define _CLIENT_H_

#include <sisci_api.h>
#include <sisci_error.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "dolphin_common.h"
#include "c63.h"

#define PRINT_COLOR "\x1B[32m"


///
/// Triggers an data interrupt with the server
/// It's purpose is to send the video width and height to the server
/// Must be called after sci_init()
///
void send_width_and_height(uint32_t width, uint32_t height);

///
/// Initializes sisci in addition to all the interrupts and dma queues being used by this program
///
void sci_init(uint32_t node);

///
/// Initializes and maps a local segment.
/// Segment size is the video frame yuv size.
/// Also connects to remote segment.
///
void sci_init_segments();

///
/// Closes all descriptors, Disconnects all remote stuff, and cleans up all segments, interrupts and dma queues.
///
void sci_close();

///
/// Begins a DMA transfer to the server with the contents of the local segment.
/// Should be used after a new frame has been written to the segment.
/// When the DMA transfer has completed, a callback function will trigger an interrupt on the server.
///
void transfer_frame();

///
/// Blocks the process until it receives an data interrupt from the server
/// The return value is the first integer that was sent with the data interrupt.
/// Expected to be used before writing encoded frame to file
///
unsigned int wait_for_encoded_image();

///
/// Triggers a data interrupt to the server
/// Available parameters:
/// SIG_CONTINUE
/// SIG_FINISHED
///
void send_signal(unsigned int signal);

///
/// Reads a YUV frame from an input video file, and writes the content into the local segment.
/// Starting address is the Y component, using ypw*wph as an offset will give you the U component, etc...
///
size_t read_frame(FILE * file);

///
/// Writes "length" bytes from the local segment to "file"
///
void write_encoded_frame(FILE * file, unsigned int length);

#endif // _CLIENT_H_
