#include <cuda_runtime.h>
#include <stdio.h>

extern "C"{
#include "cuda_me.h"
#include "cuda_dsp.h"
}


#define CUDA_CHECK_ERROR(msg) \
  do { \
    cudaError_t __err = cudaGetLastError(); \
    if (__err != cudaSuccess) { \
      fprintf(stderr, "Fatal error: %s (%s at %s:%d)\n", \
	      msg, cudaGetErrorString(__err), \
	      __FILE__, __LINE__); \
      fprintf(stderr, "*** FAILED - ABORTING\n"); \
      exit(1); \
    } \
  } while (0)



__global__ void sad_block_8x8_test(uint8_t * orig, uint8_t * recons, struct macroblock * mb, int w, int top, int left, int mx, int my, int * best_sad)
{
	int x = threadIdx.x + left;
	int y = blockIdx.x + top;
	//block1 offset calculation
	uint8_t * BlockSad1 = orig + my*w + mx;
	//block2 offset calculation
	uint8_t * BlockSad2 = recons + y*w + x;

	int u, v;
	int sad=0;

	for (v = 0; v < 8; ++v)
	{
		for (u = 0; u < 8; ++u)
		{
			sad += abs(BlockSad2[v*w + u] - BlockSad1[v*w + u]);
		}
	}

	if (sad < (*best_sad))
	{
		atomicExch(best_sad, sad);
		mb->mv_x = x - mx;
		mb->mv_y = y - my;
	}
}
__global__ void kernel_me_block_8x8(
				    uint8_t * orig,
				    uint8_t * recons,
				    struct macroblock * mbs,
				    int  search_range,
				    int  color_component,
				    int  padw,
					int  padh, int * mb_sad_values
)
{
	int mbId = blockIdx.x*blockDim.x + threadIdx.x;
	struct macroblock *mb = &mbs [mbId];

  int range = search_range;
  int mb_x = threadIdx.x;
  int mb_y = blockIdx.x;
  /* Quarter resolution for chroma channels. */
  if (color_component > 0) { range /= 2; }

  int left = mb_x * 8 - range;
  int top = mb_y * 8 - range;
  int right = mb_x * 8 + range;
  int bottom = mb_y * 8 + range;

  int w = padw;
  int h = padh;

  /* Make sure we are within bounds of reference frame. TODO: Support partial
     frame bounds. */
  if (left < 0) { left = 0; }
  if (top < 0) { top = 0; }
  if (right > (w - 8)) { right = w - 8; }
  if (bottom > (h - 8)) { bottom = h - 8; }

  int mx = mb_x * 8;
  int my = mb_y * 8;

  // new sad stretegy
  int *best_sad = mb_sad_values + mbId;
  *best_sad = INT_MAX;
  if ((bottom - top) > 0 && (right - left) > 0){
	  //initialization of grid to have thread for every single comparison of selected marcoblock with refrence image block within 16 pixcels range
	  sad_block_8x8_test<<<bottom - top, right - left>>>(orig, recons, mb, w, top, left, mx, my,best_sad);
	  cudaDeviceSynchronize();
  }
  
  mb->use_mv = 1;
}

void gpu_c63_motion_estimate(struct c63_common *cm){

  int count = cm->mb_rows * cm->mb_cols;

  size_t sizeY = cm->ypw * cm->yph;
  size_t sizeU = cm->upw * cm->uph;
  size_t sizeV = cm->vpw * cm->vph;
  
  //device pointers for orignal image
  uint8_t * device_orig_y = NULL;
  uint8_t * device_orig_u = NULL;
  uint8_t * device_orig_v = NULL;
  
  //device pointers for reconstructed image
  uint8_t * device_recons_y = NULL;
  uint8_t * device_recons_u = NULL;
  uint8_t * device_recons_v = NULL;

  //device pointers for macroblocks
  struct macroblock * device_mbs_y = NULL;
  struct macroblock * device_mbs_u = NULL;
  struct macroblock * device_mbs_v = NULL;

  int  device_search_range ;
  int  device_color_component ;
  int  device_padw ;
  int  device_padh ;

  //device pointers for sadcalculations per macroblocks
  int * device_mb_sad_values_y = NULL;
  int * device_mb_sad_values_u = NULL;
  int * device_mb_sad_values_v = NULL;
  
  cudaError_t err = cudaSuccess;

  // Malloc orig
  err = cudaMalloc((void**)&device_orig_y, sizeY);
  err = cudaMalloc((void**)&device_orig_u, sizeU);
  err = cudaMalloc((void**)&device_orig_v, sizeV);
  CUDA_CHECK_ERROR("Failed to malloc");

  // Malloc recons
  err = cudaMalloc((void**)&device_recons_y, sizeY);
  err = cudaMalloc((void**)&device_recons_u, sizeU);
  err = cudaMalloc((void**)&device_recons_v, sizeV);
  CUDA_CHECK_ERROR("Failed to malloc");

  // Malloc macroblocks
  err = cudaMalloc((void**)&device_mbs_y, count * sizeof(struct macroblock));
  err = cudaMalloc((void**)&device_mbs_u, count/4 * sizeof(struct macroblock));
  err = cudaMalloc((void**)&device_mbs_v, count/4 * sizeof(struct macroblock));
  CUDA_CHECK_ERROR("Failed to malloc");

  //Malloc sad values per macroblock
  cudaMalloc((void**)&device_mb_sad_values_y, sizeof(int)* count);
  cudaMalloc((void**)&device_mb_sad_values_u, sizeof(int)* count/4);
  cudaMalloc((void**)&device_mb_sad_values_v, sizeof(int)* count/4);
  CUDA_CHECK_ERROR("Failed to malloc");
  
  // Copy orig to device
  err = cudaMemcpy(device_orig_y, cm->curframe->orig->Y, sizeY, cudaMemcpyHostToDevice);
  err = cudaMemcpy(device_orig_u, cm->curframe->orig->U, sizeU, cudaMemcpyHostToDevice);
  err = cudaMemcpy(device_orig_v, cm->curframe->orig->V, sizeV, cudaMemcpyHostToDevice);
  CUDA_CHECK_ERROR("Failed to memcpy");

  // Copy recons to device
  err = cudaMemcpy(device_recons_y, cm->refframe->recons->Y, sizeY, cudaMemcpyHostToDevice);
  err = cudaMemcpy(device_recons_u, cm->refframe->recons->U, sizeU, cudaMemcpyHostToDevice);
  err = cudaMemcpy(device_recons_v, cm->refframe->recons->V, sizeV, cudaMemcpyHostToDevice);
  CUDA_CHECK_ERROR("Failed to memcpy");

  int color_component = Y_COMPONENT;
  
  device_search_range=cm->me_search_range;
  device_color_component=color_component; 
  device_padw = cm->ypw;
  device_padh = cm->yph;
  
  CUDA_CHECK_ERROR("Failed to memcpy");
  //launching kernel with blocks equal to macro block rows and threads equal to macro block collumns 
  kernel_me_block_8x8<<<cm->mb_rows, cm->mb_cols>>>(device_orig_y, device_recons_y, device_mbs_y, device_search_range, device_color_component, device_padw, device_padh,device_mb_sad_values_y);
  //cudaDeviceSynchronize();
  CUDA_CHECK_ERROR("Kernel_me_block_8x8 failed.");

  // Copy macroblocks back into memory
  cudaMemcpy(cm->curframe->mbs[Y_COMPONENT], device_mbs_y, count * sizeof(struct macroblock), cudaMemcpyDeviceToHost);

  //changing the count of macroblocks for u and v image
  count /= 4;
  device_padw = cm->upw;
  device_padh = cm->uph;

  color_component = U_COMPONENT;
  device_color_component = color_component;
  //launching kernel with blocks equal to macro block rows and threads equal to macro block collumns for u part of the image
  kernel_me_block_8x8<<<cm->mb_rows / 2, cm->mb_cols / 2>>>(device_orig_u, device_recons_u, device_mbs_u, device_search_range, device_color_component, device_padw, device_padh,device_mb_sad_values_u);
  //cudaDeviceSynchronize();
  CUDA_CHECK_ERROR("Kernel_me_block_8x8 failed.");

  // Copy macroblocks back into memory
  cudaMemcpy(cm->curframe->mbs[U_COMPONENT], device_mbs_u, count * sizeof(struct macroblock), cudaMemcpyDeviceToHost);

  CUDA_CHECK_ERROR("Failed to memcpy");

  device_padw = cm->vpw;
  device_padh = cm->vph;

  color_component = V_COMPONENT;
  device_color_component = color_component;
  //launching kernel with blocks equal to macro block rows and threads equal to macro block collumns for v part of the image
  kernel_me_block_8x8<<<cm->mb_rows / 2, cm->mb_cols / 2>>>(device_orig_v, device_recons_v, device_mbs_v, device_search_range, device_color_component, device_padw, device_padh,device_mb_sad_values_v);
  //cudaDeviceSynchronize();
  CUDA_CHECK_ERROR("Kernel_me_block_8x8 failed.");

  // Copy macroblocks back into memory
  cudaMemcpy(cm->curframe->mbs[V_COMPONENT], device_mbs_v, count * sizeof(struct macroblock), cudaMemcpyDeviceToHost);

  CUDA_CHECK_ERROR("Failed to memcpy");

  //memory de-allocations
  cudaFree(device_orig_y);
  cudaFree(device_orig_u);
  cudaFree(device_orig_v);

  cudaFree(device_recons_y);
  cudaFree(device_recons_u);
  cudaFree(device_recons_v);

  cudaFree(device_mbs_y);
  cudaFree(device_mbs_u);
  cudaFree(device_mbs_v);

  cudaFree(device_mb_sad_values_y);
  cudaFree(device_mb_sad_values_u);
  cudaFree(device_mb_sad_values_v);
}


#ifdef NO_CUDA

/* Motion compensation for 8x8 block */
static void gpu_mc_block_8x8(struct c63_common *cm, int mb_x, int mb_y,
                         uint8_t *predicted, uint8_t *ref, int color_component)
{
  struct macroblock *mb =
    &cm->curframe->mbs[color_component][mb_y*cm->padw[color_component]/8+mb_x];

  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = cm->padw[color_component];

  /* Copy block from ref mandated by MV */
  int x, y;

  for (y = top; y < bottom; ++y)
    {
      for (x = left; x < right; ++x)
        {
          predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
        }
    }
}

#else

__global__ void kernel_mc_block_8x8(uint8_t * predicted, uint8_t * ref, int color_component, struct macroblock * mbs, int * padw){
  int mb_x = threadIdx.x;
  int mb_y = blockIdx.x;
  //getting macroblock pointer
  struct macroblock *mb =&mbs[mb_y*padw[color_component]/8+mb_x];
						    
  if (!mb->use_mv) { return; }

  int left = mb_x * 8;
  int top = mb_y * 8;
  int right = left + 8;
  int bottom = top + 8;

  int w = padw[color_component];

  // Copy block from ref mandated by MV 
  int x, y;

  for (y = top; y < bottom; ++y)
    {
      for (x = left; x < right; ++x)
        {
          predicted[y*w+x] = ref[(y + mb->mv_y) * w + (x + mb->mv_x)];
        }
    }
}

#endif

void gpu_c63_motion_compensate(struct c63_common *cm)
{

#ifndef NO_CUDA

  int count = cm->mb_rows * cm->mb_cols;

  size_t sizeY = cm->ypw * cm->yph;
  size_t sizeU = cm->upw * cm->uph;
  size_t sizeV = cm->vpw * cm->vph;

  //device pointers for predicted image
  uint8_t * device_predicted_y;
  uint8_t * device_predicted_u;
  uint8_t * device_predicted_v;
  //allocation for predicted image
  cudaMalloc((void **)&device_predicted_y, sizeY);
  cudaMalloc((void **)&device_predicted_u, sizeU);
  cudaMalloc((void **)&device_predicted_v, sizeV);

  CUDA_CHECK_ERROR("Cuda failed to malloc");

  //device pointers for reconstructed image
  uint8_t * device_recons_y = NULL;
  uint8_t * device_recons_u = NULL;
  uint8_t * device_recons_v = NULL;
  //allocation for reconstructed image
  cudaMalloc((void **)&device_recons_y, sizeY);
  cudaMalloc((void **)&device_recons_u, sizeU);
  cudaMalloc((void **)&device_recons_v, sizeV);
  CUDA_CHECK_ERROR("Cuda failed to malloc");

  //copy reconstructed image to device
  cudaMemcpy(device_recons_y, cm->refframe->recons->Y, sizeY, cudaMemcpyHostToDevice);
  cudaMemcpy(device_recons_u, cm->refframe->recons->U, sizeU, cudaMemcpyHostToDevice);
  cudaMemcpy(device_recons_v, cm->refframe->recons->V, sizeV, cudaMemcpyHostToDevice);
  CUDA_CHECK_ERROR("Cuda failed to memcpy");

  //device pointers for marcoblocks
  struct macroblock * device_mbs_y = NULL;
  struct macroblock * device_mbs_u = NULL;
  struct macroblock * device_mbs_v = NULL;
  //marcoblocks allocations
  cudaMalloc((void**)&device_mbs_y, count * sizeof(struct macroblock));
  cudaMalloc((void**)&device_mbs_u, count/4 * sizeof(struct macroblock));
  cudaMalloc((void**)&device_mbs_v, count/4 * sizeof(struct macroblock));
  CUDA_CHECK_ERROR("Cuda failed to malloc");

  //marcoblocks copy to device
  cudaMemcpy(device_mbs_y, cm->curframe->mbs[Y_COMPONENT], count * sizeof(struct macroblock), cudaMemcpyHostToDevice);
  cudaMemcpy(device_mbs_u, cm->curframe->mbs[U_COMPONENT], count/4 * sizeof(struct macroblock), cudaMemcpyHostToDevice);
  cudaMemcpy(device_mbs_v, cm->curframe->mbs[V_COMPONENT], count/4 * sizeof(struct macroblock), cudaMemcpyHostToDevice);
  CUDA_CHECK_ERROR("Cuda failed to memcpy");

  int * device_padw = NULL;

  cudaMalloc((void**)&device_padw, sizeof(int) * 3);

  CUDA_CHECK_ERROR("Cuda failed to malloc");

  cudaMemcpy(device_padw, cm->padw, sizeof(int) * 3, cudaMemcpyHostToDevice);

  CUDA_CHECK_ERROR("Cuda failed to memcpy");
  //launching kernel with blocks equal to macro block rows and threads equal to macro block collumns 
  kernel_mc_block_8x8<<<cm->mb_rows, cm->mb_cols>>>(device_predicted_y, device_recons_y, Y_COMPONENT, device_mbs_y, device_padw);

  CUDA_CHECK_ERROR("Cuda failed during kernel call");

  cudaMemcpy(cm->curframe->predicted->Y, device_predicted_y, sizeY, cudaMemcpyDeviceToHost);

  CUDA_CHECK_ERROR("Cuda failed to memcpy");
  //launching kernel with blocks equal to macro block rows and threads equal to macro block collumns for u part of the image
  kernel_mc_block_8x8<<<cm->mb_rows / 2, cm->mb_cols / 2>>>(device_predicted_u, device_recons_u, U_COMPONENT, device_mbs_u, device_padw);

  CUDA_CHECK_ERROR("Cuda failed during kernel call");

  cudaMemcpy(cm->curframe->predicted->U, device_predicted_u, sizeU, cudaMemcpyDeviceToHost);

  CUDA_CHECK_ERROR("Cuda failed to memcpy");
  //launching kernel with blocks equal to macro block rows and threads equal to macro block collumns for v part of the image
  kernel_mc_block_8x8<<<cm->mb_rows / 2, cm->mb_cols / 2>>>(device_predicted_v, device_recons_v, V_COMPONENT, device_mbs_v, device_padw);

  CUDA_CHECK_ERROR("Cuda failed during kernel call");

  cudaMemcpy(cm->curframe->predicted->V, device_predicted_v, sizeV, cudaMemcpyDeviceToHost);

  CUDA_CHECK_ERROR("Cuda failed to memcpy");


  //memory de-allocations
  cudaFree(device_predicted_y);
  cudaFree(device_predicted_u);
  cudaFree(device_predicted_v);

  cudaFree(device_recons_y);
  cudaFree(device_recons_u);
  cudaFree(device_recons_v);

  cudaFree(device_mbs_y);
  cudaFree(device_mbs_u);
  cudaFree(device_mbs_v);

  cudaFree(device_padw);

#else

  int mb_x, mb_y;

  /* Luma */
  for (mb_y = 0; mb_y < cm->mb_rows; ++mb_y)
    {
      for (mb_x = 0; mb_x < cm->mb_cols; ++mb_x)
        {
          gpu_mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->Y,
                       cm->refframe->recons->Y, Y_COMPONENT);
        }
    }

  /* Chroma */
  for (mb_y = 0; mb_y < cm->mb_rows / 2; ++mb_y)
    {
      for (mb_x = 0; mb_x < cm->mb_cols / 2; ++mb_x)
        {
          gpu_mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->U,
                       cm->refframe->recons->U, U_COMPONENT);
          gpu_mc_block_8x8(cm, mb_x, mb_y, cm->curframe->predicted->V,
                       cm->refframe->recons->V, V_COMPONENT);
        }
    }

#endif

}