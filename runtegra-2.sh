#!/bin/bash

if [ $# -eq 0 ]; then
    ./run.sh --tegra tegra-2 --args "/mnt/sdcard/foreman.yuv -o output.y4m -w 352 -h 288"
elif [ $1 == "decode" ]; then # Requires c63dec in tegra-build directory
    tegra-build/c63dec tegra-build/output.y4m decoded.y4m
elif [ $1 == "tractor" ]; then
    ./run.sh --tegra tegra-2 --args "/mnt/sdcard/tractor.yuv -o output.y4m -w 1920 -h 1080 -f 5"
fi

